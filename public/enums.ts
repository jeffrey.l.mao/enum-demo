// Enum Demo
// 2020.09.02

// INTRO:
// Enums are a new innovation in technology: We have email, ebooks, ebikes... now, electronic numbers!
// 
// 🙃
// 
// Addl readings:
// https://www.typescriptlang.org/docs/handbook/enums.html
// https://blog.logrocket.com/writing-readable-code-with-typescript-enums-a84864f340e9/
// https://basarat.gitbook.io/typescript/type-system/enums
 
enum VoxelTeamMembers { 
    Anton, // default starts at 0, but you can start at any number you want, following enum values continue from that number
    Bartosz,
    Emil,
    Jeff,
    Joel,
    Maccesch,
    Manuel,
    Robin, // = 11,
    Rowin, // if prev value set to 11, this is 12
    Saulo, // = 'Saulo Vargas', // forgot to mention, number/strings can be mix&matched but use case would be rare, and...
    //Typer, // "Error: Enum member must have initializer.(1061)"; quickfixes are: Typer = 14, Typer = 'Typescript coder', 
    // (cont) if you initialise one string value in the enum, every value after it must also be initialised
}

let hello;
greetColleague( 12 );

function greetColleague( coder: VoxelTeamMembers ) {
    let hello;

    switch ( coder ) {
        // standard syntax
        case VoxelTeamMembers.Bartosz:
            hello = 'dzień dobry';
            break;    

        case VoxelTeamMembers.Emil:
            hello = 'Hallå';
            break;

        case VoxelTeamMembers['Jeff']:
            hello = 'nihao';
            break;
        
        case VoxelTeamMembers['Maccesch']:
            hello = 'Hallo';
            break;

        // reverse mapping
        case 8:
        case 9:
        case 10:
            hello = 'Hola';
            break;

        default:
            hello = 'Sorry, what langauge do you speak?';
    }

    console.log( `${hello} ${coder}!` );
    console.log( `oh sorry, your name is: ${ VoxelTeamMembers[coder] }\n`);

    console.log( `please greet all the other team members: ${ Object.keys( VoxelTeamMembers ) }`); // not quite what's expected
    console.log( VoxelTeamMembers ); // examine actual object; typescript enum implementation transpiles an 'encompassing' object~ (there's probably a better word for that...)
}



// enum vs union type
type WeekDay = 'Monday' | 'Wednesday';
let day: WeekDay = 'Wednesday';
// attendTeamMeeting(day);

// string-typed enum
enum DayOfWeek {
    'mon' = 'Monday',
    'wed' = 'Wednesday',
}
attendTeamMeeting( DayOfWeek.wed ); //'wed' as DayOfWeek ); // alternate syntax

function attendTeamMeeting( day: DayOfWeek ) { // day: WeekDay ) {
    console.log( `It's ${ day }; time to attend team meeting.` );
    
    console.log( `What days do we have team meetings on again?` );

    // console.log( `\nCan we get type info of our argument 'day'? "[${typeof day}]" \nNope. Ok, down the rabbit hole...`);

    // // https://stackoverflow.com/questions/41749196/enumerate-typescript-string-literals
    // let teamMeetingDays = { Monday: '', Wednesday: ''}; // values do not matter
    // console.log( Object.keys( teamMeetingDays ) );
    // console.log( `Now we know the possible values of our union type; but how do we actually use this type?` );
    // type teamMeetingDay = keyof typeof teamMeetingDays;
    // let x: teamMeetingDay = "Monday"; 
    // console.log( x );
    
    console.log( `Team meeting days: ${ Object.values( DayOfWeek ) }`);

}
